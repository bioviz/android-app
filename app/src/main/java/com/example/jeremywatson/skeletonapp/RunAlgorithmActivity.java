package com.example.jeremywatson.skeletonapp;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat4;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;

public class RunAlgorithmActivity extends AppCompatActivity {
    private String newFileName = "newImgYay.jpg";
    private int textSize = 20;
    private Switch incDecrSwitch;


    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i("Debug", "OpenCV loaded successfully");
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_algorithm);

        if (!OpenCVLoader.initDebug()) {
            Log.d("Debug", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d("Debug", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

        Intent intent = getIntent();
        String absImgPath = intent.getStringExtra(MainActivity.EXTRA_IMG);
        String dirPath = absImgPath.substring(0, absImgPath.lastIndexOf('/'));

        // Need to make this return a list of points
        int colonyCount = cvAlgorithm(absImgPath, dirPath);

        TextView text = (TextView) findViewById(R.id.colony_count);
        text.setTextSize(textSize);
        text.setText("Colony Count: " + colonyCount);

        incDecrSwitch = (Switch) findViewById(R.id.IncrementDecrementSwitch);
        incDecrSwitch.setChecked(false);
        incDecrSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                 if (isChecked) {
                     Log.v("Debug", "is in increment state");
                 } else {
                     Log.v("Debug", "is in decrement state");
                 }
             }
         });

        ImageView imageView = (ImageView) findViewById(R.id.imageViewGallery);
        imageView.setImageBitmap(BitmapFactory.decodeFile(absImgPath));
    }

                // Need to make this return a list of points
                // Allow another function to draw the circles

    protected int cvAlgorithm(String filePath, String destPath) {
        Log.v("Debug", "Starting function call");
        Mat baseImage = new Mat();
        Mat hsvImage = new Mat();
        Mat threshImage = new Mat();
        Mat finalImage = baseImage;

        File directory = new File(Environment.getExternalStorageDirectory()+File.separator+"BioVizDir");
        if (directory.mkdirs()) {
            Log.v("Yay", "Yayy directory created\n");
        }
        else {
            Log.v("Yay", "JK get rekt\n");
        }

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File outputFile = new File(path, newFileName);
        String outputFilePath = outputFile.toString();
        Log.v("Yay", "External Storage Pub Dir: " + outputFilePath.toString());

        baseImage = Imgcodecs
                .imread(filePath, Imgcodecs.CV_LOAD_IMAGE_COLOR);
        Imgproc.cvtColor(baseImage, hsvImage, Imgproc.COLOR_BGR2HSV);

        Core.inRange(hsvImage, new Scalar(0, 48, 200), new Scalar(255, 255, 255),
                threshImage);

        MatOfFloat4 circles = new MatOfFloat4();
        Imgproc.HoughCircles(threshImage, circles, Imgproc.HOUGH_GRADIENT, 1, 5,
                100, 10, 25, 100);

        for (int i = 0; i < circles.cols(); i++) {
            Point center = new Point((int) circles.get(0, i)[0],
                    (int) circles.get(0, i)[1]);
            int radius = (int) circles.get(0, i)[2];
            Imgproc.circle(finalImage, center, 3, new Scalar(0, 255, 0), 3, 8, 0);
            Imgproc.circle(finalImage, center, radius, new Scalar(0, 0, 255), 3,
                    8, 0);
        }

        boolean val = Imgcodecs.imwrite(destPath + "/" + newFileName, finalImage);
        Log.v("Debug", "imwrite succeed?: " + val);
        Log.v("Debug", "Path in CV Algorithm: " + destPath + "/" + newFileName);
        return circles.cols(); // Is there a way to convert this into an arraylist?
    }

}
