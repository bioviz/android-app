package com.example.jeremywatson.skeletonapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    public final static String EXTRA_IMG = "com.mycompany.myfirstapp.IMG_PATH";
    private static final int SELECT_PICTURE = 1;
    private static final int REQUEST_TAKE_PHOTO = 2;
    final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
    public static int count = 0;
    private String selectedImagePath;
    private String filemanagerstring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void setFileName(String filename) {
        File directory = Environment.getExternalStorageDirectory();
        System.out.println("Directory is " + directory.toString());
    }

    public void selectPicture(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), SELECT_PICTURE);
    }

    public void takePicture(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI.getPath());
        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
        selectedImagePath = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.getPath();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent intent;
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();

                //OI FILE Manager
                filemanagerstring = selectedImageUri.getPath();

                //MEDIA GALLERY
                selectedImagePath = getPath(selectedImageUri);

                //NOW WE HAVE OUR WANTED STRING
                if(selectedImagePath!=null) {
                    System.out.println("selectedImagePath is the right one for you!");
                }
                else if (filemanagerstring!=null){
                    System.out.println("filemanagerstring is the right one for you!");
                    selectedImagePath = filemanagerstring;
                }
                else {
                    System.out.println("Both strings are NULL you're screwed");
                }

            }else if( requestCode == REQUEST_TAKE_PHOTO){
                selectedImagePath = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.getPath();
            }
            else{
                Log.v("Debug","Unsuccessful Result Code");
            }
            // After the path is found, start a new activity
            // then pass the file to the CV algorithm
            intent = new Intent(this, RunAlgorithmActivity.class);
            intent.putExtra(EXTRA_IMG, selectedImagePath);
            startActivity(intent);
        }
    }

    /**
     * Helper method to retrieve the path of an image URI
     */
    private String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if(cursor!=null)
        {
            //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        else return null;
    }

}